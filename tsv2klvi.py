#!/usr/bin/python3
"""
tsv2klvi - insert metadata into an mpeg-TS file as a KLV stream

Copyright 2020 by Alec Murphy, MIT licensed | https://gitlab.com/almurphy
"""
import binascii
import sys
import os.path
import logging
import mpegTSlite
import misbKLV

"""
Simple strategy:
- read PAT and PMT from video file, patch PMT to insert KLV stream
- pass-through all other packets untouched
- do this until first PCR value is acquired

then, in parallel:
- using video PCR as reference clock
- using KLV timestamp (column 2, microsec) as KLV clock
- generate KLV stream packets and insert them so that KLV clock is
  in sync with the video PCR, like so:
    - generate the next KLV packet in advance and record its timestamp
    - pass-through packets from the original file until PCR value
      reaches KLV timestamp
    - emit the KLV packet
    - repeat in a continuous loop
- until the first end of file is reached
"""


def pktPassThrough(packet):
    """ Pass through a packet, unchanged, to output file """
    global outFile
    outFile.write(packet)


def pktPatchPMT(packet):
    """ Patch PMT table with KLV stream, then write to output file """
    global outFile
    structPMT = r.structPMT

    # append our new stream to streams table
    structPMT['streams'].append(
        {'esType': 0x15, 'esPID': klvPID,
         'esInfo': binascii.unhexlify('05044b4c564126090100ff4b4c5641000f')})

    pmtOut = mpegTSlite.tsBuildPMT(structPMT['PCRPID'], structPMT['streams'])
    outFile.write(r.buildPXTPacket(r.pmtPID, pmtOut))


structPMT = None
pmtPID = 0x1000

pcrStart = -1  # Initial PCR value
ptsStart = -1  # Initial PTS value

pcrNow = -1
tsvline = ''

klvPID = 0x111

if (len(sys.argv) != 4):
    sys.exit('Syntax: ' + sys.argv[0] + ' infile.mpg infile.tsv outfile.mpg')

mpgFileName = sys.argv[1]
tsvFileName = sys.argv[2]
outFileName = sys.argv[3]

try:
    mpgFile = open(mpgFileName, mode='rb')
except:
    sys.exit('Unable to read from: ' + mpgFileName)

try:
    tsvFile = open(tsvFileName, mode='rb')
except:
    sys.exit('Unable to read from: ' + tsvFileName)

if os.path.isfile(outFileName):
    sys.exit('Output file exists: ' + outFileName)

try:
    outFile = open(outFileName, mode='wb')
except:
    sys.exit('Unable to write to: ' + outFileName)

klvCount = 0
klvPES = ''  # Buffer for prepared KLV packet awaiting insertion

logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.INFO)

# Enable debug output
DEBUG_LEVEL = int(os.getenv('DEBUG', '0'))
if DEBUG_LEVEL:
    logger.setLevel(logging.DEBUG)


r = mpegTSlite.mpegTS()

# Set up packet callbacks
r.onRxRawPAT = pktPassThrough   # Pass though PAT and PES packets
r.onRxRawPES = pktPassThrough
r.onRxRawPMT = pktPatchPMT      # Filter PMT via our patching function

while True:

    ### VIDEO PACKET HANDLING ###

    tsPacket = mpgFile.read(mpegTSlite.PACKETSIZE)
    if len(tsPacket) <= 0:
        break

    r.ingest(tsPacket)

    # wait until first PCR reading - required for synchronizing KLV stream
    if r.pcrStartB < 0:
        continue

    ### KLV PACKET GENERATION AND INSERTION ###

    if (len(klvPES) == 0):  # No packet waiting, so create a new one
        # Start by reading a line from text file, skipping comments
        gotLine = False
        while not gotLine:
            tsvLine = tsvFile.readline()
            if not tsvLine:
                break
            if tsvLine[:1] != b'#':
                gotLine = True

        if not gotLine:
            print('TSV FILE EOF')
            break

        tsvLine = tsvLine.decode('utf8')[:-1]

        # With the line that we've read, generate a KLV packet
        kpak = misbKLV.makeKLVSyn(tsvLine)

        # use KLV timestamps to generate a PTS
        # for this, we convert from microseconds to PCR/PTS units
        pts = int(misbKLV.klvClock * 90000.0/1000000)
        logger.debug('CLK=%f, PTS=%d' % ((1.0*misbKLV.klvClock/1000000), pts))

        if ptsStart < 0:
            ptsStart = pts

        klvPTS = pts - ptsStart + r.pcrStartB

        # With KLV packet generated above and PTS value just calculated,
        # build a PES payload unit
        klvPES = mpegTSlite.buildPES(kpak, 0xfc, 0x8480,
                                     pts - ptsStart + r.pcrStartB)

    logger.debug('KLVPTS=%d, NOWPCR=%d' % (klvPTS, r.pcrBase))

    # Then we wait until PCR reaches klvPTS, and insert the packet
    if klvPTS <= r.pcrBase:
        logger.debug("INSERT KLV PES LEN=%d" % len(klvPES))

        tsPacks = r.tsPacketize(klvPID, klvPES)
        for pak in tsPacks:
            outFile.write(pak)

        klvPES = ''
        klvCount += 1

logger.info('### KLV INSERTIONS: %d' % klvCount)

outFile.close()
