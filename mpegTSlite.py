#!/usr/bin/python3 -u
"""
Copyright 2020 by Alec Murphy, MIT licensed | https://gitlab.com/almurphy

mpegTSlite module
-----------------
Pure Python implementation of a very limited subset of MPEG-TS functions,
SPARINGLY CHERRY-PICKED for the specific purposes of this software,
(that is: extraction and insertion of KLV metadata in MPEG-TS streams)
with a view to minimizing code size and dependencies, and maximizing the
fits-in-head factor, as the author is not an MPEG expert.
"""

from binascii import hexlify, unhexlify
from struct import pack, unpack
import sys
import logging

PACKETSIZE = 188

# https://en.wikipedia.org/wiki/MPEG_transport_stream
# https://en.wikipedia.org/wiki/Packetized_elementary_stream

#############################################################
# FROM MPEGTOOLS, PORTABLE WITH NO DEPS
#############################################################


def ptsDecode(buf):
    """ Decode a PTS field from 5 bytes to integer """
    (ptsA, ptsB, ptsC) = unpack('>BHH', buf[:5])
    pts = (((ptsA >> 1) & 0x07) << 30 |
           ((ptsB >> 1) & 0x7FFF) << 15 |
           ((ptsC >> 1) & 0x7FFF))
    return pts


def ptsEncode(pts):
    """ Decode a PTS field from integer to 5 bytes """
    return pack('>BHH',
                ((pts >> 30 & 0x07) << 1) | 0x21,
                ((pts >> 15 & 0x7FFF) << 1) | 0x01,
                ((pts & 0x7FFF) << 1) | 0x01)


def buildPES(payload, streamID, flags, pts=-1):
    """ Encapsulate a payload into a PES packet """
    # "Start code"
    buf = unhexlify('000001') + pack('B', streamID)

    # XXXX PES len, XXXX flags, XX headerlen, XXXXXXXXXX PTS
    if (pts >= 0):
        buf += pack('>HHB', len(payload) + 8, flags, 0x05)
        buf += ptsEncode(pts)
    else:
        # NO PTS - !!!WARNING UNTESTED!!!
        buf += pack('>HHB', len(payload) + 3, flags, 0x00)

    buf += payload
    return (buf)


# END OF MPEGTOOLS
#############################################################

#############################################################
# FROM MPEG_PSI, PORTABLE WITH NO DEPS
#############################################################


def verbosePSI(s):
    try:
        return logger.debug(s)
    except NameError:
        #return print(s)
        pass


def crc32mpeg2(message):
    """ CRC32 algorithm, MPEG2 flavour (used in PAT and PMT blocks) """

    crc = 0xFFFFFFFF
    for i in range(len(message)):
    # xor next byte to upper bits of crc
        if(isinstance(message, str)):
            crc ^= ord(message[i]) << 24
        else:
            crc ^= (message[i] & 0xff) << 24

        for j in range(8):    # Do eight times.
            msb = crc >> 31
            crc <<= 1
            crc ^= (0 - msb) & 0x04C11DB7
            crc &= 0xFFFFFFFF

    return crc


# pcrPID = which PID to use for clock reference
# If this is unused. then set to 0x1FFF (all bits on)
def tsBuildPMT(pcrPID=0x1fff, eStreams={}):
    """ Given the PCR PID and a dict of streams, build a PMT packet """

    # "PMT specific data" - mainly PCR PID
    secFlags = 0xc1
    pmt = pack('>HBBBHH', 1, secFlags, 0, 0, pcrPID | 0xe000, 0 | 0xf000)

    for s in eStreams:
        # Elementary stream specific data repeated until end of section length
        esData = pack('>BHH',
                      s['esType'],
                      s['esPID'] | 0xe000,
                      len(s['esInfo']) | 0xf000)
        esData += s['esInfo']
        pmt += esData  # append to PMT
        verbosePSI('ES: ' + hexlify(esData).decode('utf8'))

    # prepend header and length
    pmt = pack('>BH', 2, (len(pmt) + 4) | 0xb000) + pmt

    # append CRC
    pmt += pack('>I', crc32mpeg2(pmt))

    # pointer field always zero
    return(b'\x00' + pmt)


def tsParsePAT(pay):
    """ Parse a PAT packet, return PMT PID """

    verbosePSI('PAT PACKET DECODING:')

    # First byte is an offset pointer, should be always zero
    payPos = 1

    #local = readFile(filehandle,k,4)
    (tableID, tableFlags, tableLen) = unpack('BBB', pay[payPos:payPos+3])

    if (tableID != 0x0):
        verbosePSI('INVALID TABLE ID, expected 0, found %d' % tableID)
        return

    section_length = tableLen | ((tableFlags & 0x0f) << 8)
    verbosePSI('section_length=0x%x' % section_length)
    payPos += 3

    (tsID, secFlags, secNum, secLast) = unpack('>HBBB', pay[payPos:payPos+5])

    # FIXME: some bitmap flags ignored, will come back to bite us
    verbosePSI('TS_ID=0x%04x, sect_number=%d, sect_last=%d' %
               (tsID, secNum, secLast))
    payPos += 5

    tableBytes = section_length - 4 - 5

    while (tableBytes > 0):
        (progNumber, progMapPID) = unpack('>HH', pay[payPos:payPos+4])
        progMapPID &= 0x1fff
        verbosePSI('  progNumber = 0x%x' % progNumber)
        if (progNumber == 0):
            verbosePSI('  networkPID = 0x%x' % progMapPID)
        else:
            verbosePSI('  progMapPID = 0x%x' % progMapPID)
        tableBytes = tableBytes - 4
        payPos += 4

    includedCRC = unpack('>I', pay[payPos:payPos+4])[0]
    computedCRC = crc32mpeg2(pay[1:payPos])
    if (includedCRC != computedCRC):
        print('CRC FAILED! WANT: %x, HAVE: %x' % (includedCRC, computedCRC))

    return(progMapPID)


def tsParsePMT(pay):
    """ Parse a PMT packet, return PCRPID and dict of streams """

    verbosePSI('PMT PACKET DECODING:')

    # First byte is an offset pointer, should be always zero
    payPos = 1

    myPMT = {}

    (tableID, tableFlags, tableLen) = unpack('BBB', pay[payPos:payPos+3])

    if (tableID != 0x2):
        verbosePSI('INVALID TABLE ID, expected 2, found %d' % tableID)
        return

    section_length = tableLen | ((tableFlags & 0x0f) << 8)
    verbosePSI('section_length=0x%x' % section_length)
    payPos += 3

    (tsID, secFlags, secNum, secLast) = unpack('>HBBB', pay[payPos:payPos+5])
    verbosePSI('prog_ID=0x%04x, sect_number=%d, last_sect_number=%d' %
               (tsID, secNum, secLast))
    payPos += 5

    (PCRPID, progInfoLen) = unpack('>HH', pay[payPos:payPos+4])
    PCRPID &= 0x1FFF
    progInfoLen &= 0xFFF
    verbosePSI('PCRPID=0x%04x, progInfoLen=%d' % (PCRPID, progInfoLen))
    payPos += 4

    myPMT['PCRPID'] = PCRPID
    myPMT['streams'] = []

    # progInfo is a TLV list which we will skip/ignore
    # WHEN CRAFTING, don't include one and set progInfoLen to 0

    tableBytes = section_length - 4 - 5 - 4 - progInfoLen

    while (tableBytes > 0):
        (esType, esPID, esInfoLen) = unpack('>BHH', pay[payPos:payPos+5])
        esPID &= 0x1FFF
        esInfoLen &= 0xFFF

        stream = {}
        stream['esType'] = esType
        stream['esPID'] = esPID
        stream['esInfo'] = pay[payPos+5:payPos+5+esInfoLen]

        # add to list of streams
        myPMT['streams'].append(stream)

        verbosePSI('  esType=0x%02x, esPid=0x%04x, esInfo Len=%d: ' %
                   (esType, esPID, esInfoLen) +
                   hexlify(stream['esInfo']).decode('utf8'))

        payPos += 5 + esInfoLen
        tableBytes -= 5 + esInfoLen

    includedCRC = unpack('>I', pay[payPos:payPos+4])[0]
    computedCRC = crc32mpeg2(pay[1:payPos])
    if (includedCRC != computedCRC):
        print('CRC FAILED! WANT: %08x, HAVE: %08x' %
              (includedCRC, computedCRC))

    return myPMT

# END OF MPEG_PSI
#############################################################


class mpegTS():
    """
    MPEG2TS class - contains all state variables for an mpeg-ts container,
    and functions for parsing and building some common data structures
    """
    pmtPID = 0x1000  # default, to be extracted from PAT (see parsePAT)

    # elementary streams (ES), each identified by a Packet Identifier (PID)
    ES = {}

    # internal representation of Program Map Table (PMT)
    structPMT = {}

    # Running values, updated from PID indicated as PCR source in the PMT
    pcrBase = -1
    pcrExtn = -1

    # Initial value of pcrBase and pcrExtn
    pcrStartB = -1
    pcrStartE = -1

    def __init__(self):
        self.logger = logging.getLogger()
        #self.logger.debug('INIT')

    def ingest(self, tsPacket):
        (syncByte, a, b, c) = unpack('BBBB', tsPacket[:4])
        if (syncByte != 0x47):
            self.logger.warn('OUT OF SYNC, CAN\'T INGEST!')
            return False

        tsPID = (a << 8 | b) & 0x1fff
        tsBits = a >> 5  # 3 bits: Error, Unit Start, Priority
        tsPUSI = (tsBits >> 1) & 0x01  # Packet Unit Start Indicator

        tsCnt = c & 0x0f
        tsFlag = (c >> 4) & 0x0f

        tsHasPay = (tsFlag & 0x01)  # TS packet contains payload?
        tsHasAda = ((tsFlag >> 1) & 0x01)   # also contains adaptation field?

        rPos = 4  # finished reading the header

        # First time, create state variables
        if tsPID not in self.ES:
            self.addPID(tsPID)

        self.ES[tsPID]['counter'] = tsCnt

        self.logger.debug('PID=%04x PUSI=%x cnt=%x ada=%x pay=%x' %
                          (tsPID, tsPUSI, tsCnt, tsHasAda, tsHasPay))

        if (tsHasAda):
            # 1 byte ada field len, 1 byte bitmap flags
            (tsAdaLen, tsAdaFlag) = unpack('BB', tsPacket[4:6])
            rPos = 6

            self.logger.debug('adaLen=%d adaFlag=%x' %
                              (tsAdaLen, tsAdaFlag))
            if (tsAdaFlag == 0):
                self.logger.debug('  (probably padding)')

            elif ((tsAdaLen > 6) and (tsAdaFlag & (1 << 4))):  # PCR flag
                # Program clock reference: 33b base, 6b reserved, 9b extension
                """
                WIKIPEDIA says:
                  "The value is calculated as base * 300 + extension"
                THIS IS INCOMPLETE (a bitshift not shown)
                Correct expression see discussion:
                  https://stackoverflow.com/questions/6199940

                For our purposes, the base precision is sufficient:
                -> assume PCR EXT always 0, or carry it forward as-is
                """
                (tsPCRa, tsPCRb) = unpack('>IH', tsPacket[6:12])
                tsPCRBase = (tsPCRa << 1) | ((tsPCRb >> 15) & 0x01)
                tsPCRExtn = tsPCRb & 0x01ff
                self.logger.debug('  PCR=%d, PCRE=%d <-0x' %
                                  (tsPCRBase, tsPCRExtn) +
                                  hexlify(tsPacket[6:12]).decode('utf8') +
                                  ' (time=%.4fs)' % (float(tsPCRBase)/90000))

                # If we have a PMT and we know the PCR PID
                try:
                    if tsPID == self.structPMT['PCRPID']:
                        self.pcrBase = tsPCRBase
                        self.pcrExtn = tsPCRExtn
                        if self.pcrStartB < 0:
                            self.pcrStartB = tsPCRBase
                            self.pcrStartE = tsPCRExtn
                except:
                    # probably no PMT yet, or no PCR PID specified
                    pass

            rPos += tsAdaLen - 1

        if (tsHasPay):
            self.logger.debug('payLen=%d' % (PACKETSIZE - rPos))

            if (tsPID == 0x1fff):
                # PID 0x1fff is null packet for padding
                return True

            if (tsPID == 0x0000):
                # PID 0 is always PAT, it contains the PMT PID
                self.pmtPID = tsParsePAT(tsPacket[rPos:])
                return self.onRxRawPAT(tsPacket)

            if (tsPID == self.pmtPID):
                self.structPMT = tsParsePMT(tsPacket[rPos:])
                for s in self.structPMT['streams']:
                    # Initialize streams if not already
                    if s['esPID'] not in self.ES:
                        self.addPID(s['esPID'])

                    # Update pmtdata
                    self.ES[s['esPID']]['esType'] = s['esType']
                    self.ES[s['esPID']]['esInfo'] = s['esInfo']

                return self.onRxRawPMT(tsPacket)

            if (tsPUSI):
                if (tsPacket[rPos:rPos+3] == unhexlify('000001')):
                    # "Packetized Elementary Stream" - PES packet
                    (tsStream, tsPESLen) = unpack('>BH',
                                                  tsPacket[rPos+3:rPos+6])
                    self.ES[tsPID]['stream'] = tsStream

                    # Packet length FIELD can be set to zero in video streams
                    self.logger.debug('  TYPE: PES Stream=%x Len=%d' %
                                      (tsStream, tsPESLen))

                    # Read inside PES packet - "optional PES Header"
                    rPos += 6
                    (pesFlags, pesHdrLen) = unpack('>HB',
                                                   tsPacket[rPos:rPos+3])
                    rPos += 3
                    self.logger.debug('  PES Flags=%x HdrLen=%d HDR=0x' %
                                      (pesFlags, pesHdrLen) +
                                      hexlify(tsPacket[rPos:rPos+pesHdrLen]).
                                      decode('utf-8'))

                    pesPayLen = 0
                    if (tsPESLen > 0):
                        pesPayLen = tsPESLen - 3 - pesHdrLen
                        self.logger.debug('  pesPayLen=%d' % pesPayLen)

                    self.ES[tsPID]['paylen'] = pesPayLen

                    # (PTS present?)
                    if ((pesFlags & 0x0080) and (pesHdrLen >= 5)):
                        # Read "Presentation Time Stamp" PTS
                        pesPTS = ptsDecode(tsPacket[rPos:rPos+5])
                        self.logger.debug('  PTS=%d <-0x' % pesPTS +
                                          hexlify(tsPacket[rPos:rPos+5]).
                                          decode('utf-8'))
                    else:
                        pesPTS = 0

                    if ((pesFlags & 0x0040) and (pesHdrLen >= 10)):
                        # DTS also present? - not important
                        self.logger.debug('  DTS <-0x' +
                                          hexlify(tsPacket[rPos+5:rPos+10]).
                                          decode('utf-8'))

                    rPos += pesHdrLen

                    if (len(self.ES[tsPID]['buffer']) > 0):
                        # new data arrived, old data still in buffer
                        #    !!! THIS SHOULD NEVER HAPPEN !!!
                        # proc like a complete buffer but will probly fail
                        self.logger.debug('  ORPHAN BUF LEN=%d' %
                                          len(self.ES[tsPID]['buffer']))

                        # Pass faulty packet to processing
                        self.onRxFullPES(self.ES[tsPID], True)

                    self.ES[tsPID]['buffer'] = tsPacket[rPos:]
                    self.ES[tsPID]['pts'] = pesPTS

                else:
                    self.logger.debug('  TYPE: NON-PES')

            else:   # not tsPUSI
                # append fragment to stream buffer
                self.ES[tsPID]['buffer'] += tsPacket[rPos:]

            # FINALLY - Whether or not tsPUSI,
            #   if expected length was reached, pass to processing

            # HOWEVER, NOTE THAT:
            #   "A value of zero for the PES packet length can be used
            #   only when the PES packet payload is a video elementary
            #   stream"
            # So, if the stream is video, this won't work as intended
            #   see also debug message "wanted=ANY" below
            # Not a problem, as long as we don't go inside video streams
            #   (only pass them through unchanged)

            if ('esType' in self.ES[tsPID]):
                # it's an elementary stream
                if (len(self.ES[tsPID]['buffer']) >=
                        self.ES[tsPID]['paylen']):
                    # flush completed buffer
                    if (self.ES[tsPID]['paylen'] > 0):
                        self.logger.debug("  GOT BUF LEN=%d (wanted=%d)" %
                                          (len(self.ES[tsPID]['buffer']),
                                           self.ES[tsPID]['paylen']))
                    else:
                        self.logger.debug("  GOT BUF LEN=%d (wanted=ANY)" %
                                          len(self.ES[tsPID]['buffer']))

                    # Pass full packet to processing
                    self.onRxFullPES(self.ES[tsPID])

                    # Reset buffer
                    self.ES[tsPID]['buffer'] = bytes()
                    self.ES[tsPID]['paylen'] = 0

                return self.onRxRawPES(tsPacket)

            else:
                # If it's not PAT/PMT and not a PES... then generic PSI?
                # Not entirely true - should we have an "other" category?
                return self.onRxRawPSI(tsPacket)

        return True

    def onRxRawPAT(self, packet):
        """ Called by ingest when received a PAT packet """
        return True

    def onRxRawPMT(self, packet):
        """ Called by ingest when received a PMT packet """
        return True

    def onRxRawPSI(self, packet):
        """ Called by ingest when received a PSI packet, other than PAT/PMT """
        return True

    def onRxRawPES(self, packet):
        """ Called by ingest when received a PES packet """
        return True

    def onRxFullPES(self, stream, error=False):
        """ Called by ingest to process a reassembled PES packet """
        return True

    def addPID(self, PID, esType=-1, esInfo=b''):
        """ Add and initialize a new PID into our internal table;
              ES type and ES info can be supplied, but not reqd
        """
        self.ES[PID] = {}
        self.ES[PID]['buffer'] = bytes()
        self.ES[PID]['counter'] = 0
        if (esType > 0):
            self.ES[PID]['esType'] = esType
            self.ES[PID]['esInfo'] = esInfo

    def buildTSPacket(self, tsPID, tsPUSI, cnt, pay, ada=None):
        buf = pack('>BHB', 0x47,
                   (tsPID & 0x1fff) | (0x4000 * tsPUSI),
                   cnt | (0x10 if len(pay) else 0) |
                         (0x20 if (ada is not None) else 0))

        if (ada is not None):
            # Adaptation field
            # only padding type adafields currently supported (adaFlags = 0x00)
            buf += pack('BB', len(ada)+1, 0)
            buf += ada

        buf += pay
        return buf

    def buildPXTPacket(self, tsPID, pay):
        # First time? create a record
        if tsPID not in self.ES:
            self.addPID(tsPID)

        buf = pack('>BHB', 0x47,
                   (tsPID & 0x1fff) | 0x4000,
                   self.ES[tsPID]['counter'] | 0x10)

        self.ES[tsPID]['counter'] += 1
        self.ES[tsPID]['counter'] &= 0x0f

        buf += pay
        buf += (PACKETSIZE - 4 - len(pay)) * b'\xff'  # padding

        return buf

    def tsPacketize(self, tsPID, pay):
        """
        Chop and packetize an outbound PES payload into 188-byte MPEG-TS units
        """
        tPaks = []  # list of packets that we generate

        # First time? create state variables
        if tsPID not in self.ES:
            self.addPID(tsPID)

        outPos = 0
        fragCnt = 0

        while outPos < len(pay):
            # FIXME - This three-way conditional is cringe inducing
            if (len(pay) - outPos) >= (PACKETSIZE - 4):
                # full packets (no padding via ada field)
                self.logger.debug("FRAG %d payLen=%d" %
                                  (fragCnt, PACKETSIZE - 4))
                pak = self.buildTSPacket(tsPID,
                                         (outPos == 0),
                                         self.ES[tsPID]['counter'],
                                         pay[outPos:outPos+(PACKETSIZE-4)])
                outPos += PACKETSIZE-4
            elif (len(pay) - outPos) == (PACKETSIZE - 5):
                # incomplete packet, but no room for empty ada field
                self.logger.debug("FRAG %d payLen=%d" %
                                  (fragCnt, PACKETSIZE - 6))
                pak = self.buildTSPacket(tsPID,
                                         (outPos == 0),
                                         self.ES[tsPID]['counter'],
                                         pay[outPos:-1], b'')
                outPos += PACKETSIZE-6
            else:
                # last packet incomplete, padding is required
                padLen = (PACKETSIZE-4) - (len(pay) - outPos) - 2
                self.logger.debug("FRAG %d payLen=%d, padLen=%d" %
                                  (fragCnt, len(pay) - outPos, padLen))
                pak = self.buildTSPacket(tsPID, (outPos == 0),
                                         self.ES[tsPID]['counter'],
                                         pay[outPos:], padLen * b'\xff')
                outPos = len(pay)

            tPaks.append(pak)

            self.ES[tsPID]['counter'] += 1
            self.ES[tsPID]['counter'] &= 0x0f
            fragCnt += 1

        return tPaks


#############################################################
# mpegTSlite module ends here
# The code below is for testing and demo purposes
#############################################################

def testPesHandler(s, error=False):
    if ('esInfo' in s and (s['esInfo'][:6] == b'\x05\x04KLVA')):
        # It's a KLV packet
        if (s['esType'] == 0x15):
            print('SYN KLV')
        else:
            print('ASY KLV')

    # For testing, so we can pick it up from main()
    global testPesBuffer
    testPesBuffer = s['buffer']


if __name__ == "__main__":
    import os
    import random

    global testPesBuffer

    logger = logging.getLogger()
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.INFO)
    #logger.setLevel(logging.DEBUG)

    # TEST PTS encode/decode with a bunch of random numbers
    print('TESTING PTS encode/decode:')
    for i in range(10000):
        r = random.randint(0, 0x01ffffffff)
        if ptsDecode(ptsEncode(r)) != r:
            print('  FAILED for PTS=0x%x' % r)
            sys.exit()
    print('  SUCCESS')

    print(78 * '=')
    print('TESTING PMT functions:')
    # Start with a premade PMT, take it apart, put it back together, compare
    pmtBlob = unhexlify('0002b02d0001c10000e100f0001be100' +
                        'f0000fe101f00015e102f01105044b4c' +
                        '564126090100ff4b4c5641000f714d205d')
    print('  INP: ' + hexlify(pmtBlob).decode('utf-8'))
    pmtStruct = tsParsePMT(pmtBlob)
    pmtOutput = tsBuildPMT(pmtStruct['PCRPID'], pmtStruct['streams'])
    print('  OUT: ' + hexlify(pmtOutput).decode('utf8'))
    if (pmtOutput == pmtBlob):
        print('  SUCCESS')
    else:
        print('  FAILED')

    print(78 * '=')
    print('TESTING PES EN/DE-CAPSULATION:')

    # Start with a known text, encapsulate + packetize it, then
    #   parse it as if received and compare with original

    # first, intialize a mpegTS object
    testPID = 0x1234
    r = mpegTS()
    # set PES callback to our handler
    r.onRxFullPES = testPesHandler
    # the mpegTS object starts with an empty ES (elementary streams) table
    #   the following line initializes an ES for our test
    r.addPID(testPID, 0x15)  # SYN KLV

    # Do this a few times at full speed
    for i in range(5000):
        # Start with a "known text" (really a random amount of random data)
        textSize = random.randint(16, 10240)
        testMsg = os.urandom(textSize)
        testPesBuffer = b''  # where our parsed data will arrive

        # PTS - use random to properly grind all bits
        pts = random.randint(0, 0x01ffffffff)

        # ENCAPSULATE
        testPES = buildPES(testMsg, 0xfc, 0x8480, pts)

        # PACKETIZE
        for packet in r.tsPacketize(testPID, testPES):
            # Feed packets to parser to DEPACKETIZE and DECAPSULATE
            r.ingest(packet)

        # Callback function should have populated our buffer
        # Compare received data with original
        if (testMsg != testPesBuffer):
            print('  FAILED!')
            print('  INP: ' + hexlify(testMsg).decode('utf8'))
            print('  OUT: ' + hexlify(testPesBuffer).decode('utf8'))
            exit()

    print('  SUCCESS')

    r = None
    fileName = ''
    # If a filename was provided on CLI, try parsing it
    if (len(sys.argv) > 1):
        fileName = sys.argv[1]

        print(78 * '=')
        print('TESTING stream reading (file=%s):' % fileName)

        r = mpegTS()
        r.onRxFullPES = testPesHandler

        with open(fileName, mode='rb') as file:
            while True:
                tsPacket = file.read(PACKETSIZE)
                testPesBuffer = b''
                if len(tsPacket) > 0:
                    logger.debug(78 * '-')
                    r.ingest(tsPacket)
                else:
                    break
