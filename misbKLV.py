#!/usr/bin/python3
"""
Copyright 2020 by Alec Murphy, MIT licensed | https://gitlab.com/almurphy

misbKLV module
--------------
Pure Python functions for parsing and building MISB ST0601 data blocks
"""

from binascii import hexlify, unhexlify
from struct import pack, unpack
from ST0601Tab1 import keyTable

DEBUG_DUMP_RAW_KLV = 0  # dump each KLV block hexlified
DEBUG_DUMP_KLV_VARS = 0  # detail dump KLV vars
DEBUG_DUMP_KLV_TSV = 0  # dump KLV TSV lines


txt2Format = {
    'int8': 'b',
    'uint8': 'B',
    'int16': '>h',
    'uint16': '>H',
    'int32': '>l',
    'uint32': '>L',
    'int64': '>q',
    'uint64': '>Q',
}


# 6.4 LS Universal Keys
klvUniKey = unhexlify('060e2b34020b01010e01030101000000')

klvClock = 0


def verboseKLV(s):
    try:
        return logger.debug(s)
    except NameError:
        #print(s)
        pass


# 6.5.2 Tag and Length Field Encoding
def klvLenEncode(i):
    if (i < 0x80):
        return pack('B', i)
    elif (i < 0x100):
        return pack('BB', 0x81, i)
    else:  # if (i<0x10000):
        return pack('>BH', 0x82, i)
    # we could keep going, but we don't expect lengths above 65535


# 6.8 "Error Detection" - packet checksum algo
def klvCheckSum(packet):
    chkSum = 0
    for i in range(len(packet) - 2):
        if(isinstance(packet, str)):
            chkSum += ord(packet[i]) << (8 * ((i + 1) % 2))
        else:
            chkSum += packet[i] << (8 * ((i + 1) % 2))
        chkSum &= 0xffff
    return chkSum


def procKLV(packet, klvSyn=False):
    # Sanity check
    if len(packet) < 16:
        verboseKLV('Short packet passed to procKLV(), can\'t process')
        return False

    if klvSyn:
        # Can be multiple packets
        return procKLVSyn(packet)[0]
    else:
        return procKLVOne(packet)


# ST1402.1  9.4.1 Synchronous Metadata Multiplex Method
def procKLVSyn(packet):
    rPos = 0
    res = []

    while(rPos < len(packet)):
        (servID, seqNum, flags, cellLen) = unpack('>BBBH',
                                                  packet[rPos:rPos+5])
        verboseKLV('ServID: %02x, SeqNum: %02x, flags: %02x, len: %d' %
                   (servID, seqNum, flags, cellLen))
        rPos += 5
        res.append(procKLVOne(packet[rPos:rPos+cellLen]))
        rPos += cellLen

    return res


def procKLVOne(myKLV):

    verboseKLV('procKLVOne() called')

    ### Temporarily out-of-service
    #if DEBUG_DUMP_RAW_KLV:
    #    verboseKLV('# %012x ' % streams[klvPID]['pts'] +
    #          hexlify(myKLV).decode('utf8'))

    # The lists that will hold our keys and vals
    gotKeys = []
    gotVals = []

    bufPos = 0

    # Export the current packet timestamp to a global variable
    #   (useful for stream synchronization)
    global klvClock

    # Sanity check on packet 16-byte key
    if (myKLV[bufPos:bufPos+16] != klvUniKey):
        print('BAD KLV HDR')
        return False

    # Packet checksum per ST0601.8 Section 6.8
    chkSum = klvCheckSum(myKLV)
    verboseKLV('INP CSUM: %04x' % chkSum)

    # Skip over 16-byte key
    bufPos += 16

    # CHEATING! read a BER encoded integer
    bufPos += 2

    while bufPos < len(myKLV):
        # CHEATING! k and l are BER numbers (Tag and Length respectively)
        (k, l) = unpack('BB', myKLV[bufPos:bufPos+2])
        bufPos += 2

        v = myKLV[bufPos:bufPos+l]

        # By default, display hexlified
        vOut = '0x' + hexlify(v).decode('utf8')

        if (k in keyTable):
            kName = keyTable[k][1]

            # Don't hexlify ISO646 (7-bit printable) strings
            if (keyTable[k][4] == 'ISO646'):
                vOut = (b'"' + v + b'"').decode('utf8')
                gotKeys.append('%d' % k)
                gotVals.append(vOut)

            elif ((k != 1) and (keyTable[k][4] in txt2Format)):
                # use translation table to convert descriptions such as
                #  "uint32" to Python struct format such as ">L", then
                #  parse the value using struct.unpack
                try:
                    vValue = unpack(txt2Format[keyTable[k][4]], v)[0]
                except:
                    verboseKLV('Failed to unpack value: ' +
                               hexlify(v).decode('utf8') +
                               ' using format: ' + txt2Format[keyTable[k][4]])
                else:
                    # if there's a range specified, map value to range
                    if (len(keyTable[k]) > 5):
                        rgMin = keyTable[k][5][0]
                        rgMax = keyTable[k][5][1]
                        vvMax = 1 << (8 * l)

                        if (keyTable[k][4][:4] == 'uint'):
                            # unsigned value
                            rValue = (rgMin +
                                      1.0 * (rgMax-rgMin) * vValue/vvMax)
                        else:
                            # signed value, center range around origin
                            rValue = (rgMin +
                                      1.0 * (rgMax-rgMin) *
                                      (vValue + (vvMax >> 1))/vvMax)

                        vOut += ' = %f' % rValue
                        gotKeys.append('%d' % k)
                        gotVals.append('%.10f' % rValue)

                    else:
                        # no ranging required
                        vOut += ' = %d' % vValue
                        gotKeys.append('%d' % k)
                        gotVals.append('%d' % vValue)

                    # Finally, append units
                    vOut += ' ' + keyTable[k][3]

            else:  # neither printable nor int - use default hexlify
                gotKeys.append('%d' % k)
                gotVals.append(vOut)

        else:
            kName = 'UNKNOWN!'

        # Extract a copy of the timestamp - useful as an ad-hoc clocksource
        if k == 2:
            klvClock = vValue

        if DEBUG_DUMP_KLV_VARS:
            verboseKLV('    K=%02x L=%02x ' % (k, l) +
                       '(' + kName + ') ' + vOut)

        bufPos += l

    tsvKeysAndValues = ','.join(gotKeys) + '\t' + '\t'.join(gotVals)
    if DEBUG_DUMP_KLV_TSV:
        verboseKLV('Got keys-vals: ' + tsvKeysAndValues)

    return tsvKeysAndValues


# previously makeKLV
def makeKLVOne(tsvKeysAndValues):
    """
    Assemble a KLV payload from the TSV (keys+vals) one-liner described above
      uses keyTable from ST0601Tab1
    """

    # Export the current packet timestamp to a global variable
    #   (useful for stream synchronization)
    global klvClock

    # Parse and sanity-check our input
    vals = tsvKeysAndValues.split('\t')
    keys = vals.pop(0).split(',')

    if (len(keys) != len(vals)):
        verboseKLV("Number of keys and vals don't match!")
        return False

    # This is our working buffer
    tmpKLV = bytes()

    # Keys and Values lists are same length and run in parallel
    for i in range(len(keys)):
        k = int(keys[i])  # the Key
        l = 0             # the Length
        v = bytes()       # the Value
        vOut = bytes()    # the humanized value for debug output

        if (keyTable[k][4] == 'ISO646'):
            # Printable string, strip quotes and convert to bytes
            v = vals[i].strip('\"').encode('utf8')
            l = len(v)
            vOut = v

        elif ((k != 1) and (keyTable[k][4] in txt2Format)):
            # It's an integer key - requires ranging and packing

            # Read input value as a float initially
            vIn = float(vals[i])

            # Our format (description) is in column 4 of "Table1"
            # Find length by formatting a number and measuring it
            l = len(pack(txt2Format[keyTable[k][4]], 1))

            # The post-ranging value - defaults to input value
            vNorm = round(vIn)

            # However, if a range is specified, map input value to range
            if (len(keyTable[k]) > 5):
                # Min and max ends of range
                rgMin = keyTable[k][5][0]
                rgMax = keyTable[k][5][1]

                # Check input value is within range
                if (vIn < rgMin):
                    # Should throw an error
                    vIn = rgMin
                if (vIn > rgMax):
                    # Should throw an error
                    vIn = rgMax

                # The post-ranging "normalized" value
                vNorm = round((1 << (8 * l)) * 1.0 * (vIn-rgMin)/(rgMax-rgMin))

                # If converting to SIGNED integer, shift range halfway left
                if (keyTable[k][4][:4] != 'uint'):
                    vNorm -= 1 << (8 * l - 1)

            # Common for both ranged and direct integer values:
            v = pack(txt2Format[keyTable[k][4]], vNorm)
            vOut = hexlify(v)

            # Extract a copy of the timestamp - useful as an ad-hoc clocksource
            if k == 2:
                klvClock = vNorm

        elif (k == 1):
            # 16-bit checksum is calculated at the end - leave blank for now
            v = pack('>H', 0)
            l = len(v)

        else:
            # Not a string and not an integer, so we expect a hexlified blob
            if (vals[i][:2] == '0x'):
                v = unhexlify(vals[i][2:])
                l = len(v)
            else:
                # Probably an unquoted string?
                verboseKLV('Don\'t know how to convert this')
                return False

        # At this point, we have K, L and V! Pack them together
        elem = pack('BB', k, l) + v
        if DEBUG_DUMP_KLV_VARS:
            verboseKLV("    K=%02x L=%02x type " % (k, l) + keyTable[k][4] +
                       " : " + vals[i] + " -> " + vOut.decode('utf8'))

        # Append to workbuffer
        tmpKLV += elem

    # We're done, all KLV elements processed and concatenated in workbuffer
    verboseKLV('LEN=0x%x' % len(tmpKLV))

    # Prepend KLV key and (BER encoded) length to workbuffer
    tmpKLV = klvUniKey + klvLenEncode(len(tmpKLV)) + tmpKLV

    # Packet checksum per ST0601.8 Section 6.8
    chkSum = klvCheckSum(tmpKLV)
    verboseKLV('OUT CSUM: %04x' % chkSum)

    # Append checksum at the end, replacing empty placeholder
    tmpKLV = tmpKLV[:-2] + pack('>H', chkSum)

    # Show it to the world
    verboseKLV(hexlify(tmpKLV).decode('utf8'))

    return tmpKLV


# ST1402.1  9.4.1 "Synchronous Metadata Multiplex Method" and Fig.4
def makeKLVSyn(tsvKeysAndValues):
    tmpKLV = makeKLVOne(tsvKeysAndValues)
    # 0000dfXXXX
    return pack('>BBBH', 0, 0, 0xdf, len(tmpKLV)) + tmpKLV


if __name__ == "__main__":
    testTSV = '2,3,4,5,6,7,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,47,48,56,59,65,72,1	1348087826484970	"ESRI_Metadata_Collect"	"N97826"	157.5988769531	3.3905029297	-6.4910888672	"C208B"	""	""	41.0957400128	-104.8702156451	2932.9727172852	3.0651855469	1.7221069336	254.2500001006	-20.3828124795	0.0000000000	2291.8905597180	0.0000000000	41.1068024067	-104.8510062508	1867.1615600586	0.8674621582	0.2677917480	-0.0343322754	1.1787414551	-0.8171081543	-0.2494812012	0.0343322754	-1.1100769043	0	0x01010102010103042f2f4341040005000602434115100000000000000000000000000000000016020005	0.0000000000	"Firebird"	1	0	0xf905'

    print('ASY TEST')
    backTSV = procKLV(makeKLVOne(testTSV), False)
    if (testTSV == backTSV):
        print('  SUCCESS')
    else:
        print('  FAILED:')
        print('  T:' + testTSV)
        print('  B:' + backTSV)
        exit()

    print('SYN TEST')
    backTSV = procKLV(makeKLVSyn(testTSV), True)
    if (testTSV == backTSV):
        print('  SUCCESS')
    else:
        print('  FAILED:')
        print('  T:' + testTSV)
        print('  B:' + backTSV)
        exit()
