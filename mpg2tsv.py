#!/usr/bin/python3
"""
mpg2tsv - extract KLV data from an mpeg-TS file in TSV format

Copyright 2020 by Alec Murphy, MIT licensed | https://gitlab.com/almurphy
"""
import sys
import os
import logging
import mpegTSlite
import misbKLV


def myPesHandler(s, error=False):
    global asyWarned

    if (('esInfo' in s) and (s['esInfo'][:6] == b'\x05\x04KLVA')):
        # It's a KLV packet

        if len(s['buffer']) == 0:
            logger.warn('zero length KLV buffer')
            # ignore zero size packets (these exist)
            return

        oneTsv = misbKLV.procKLV(s['buffer'], (s['esType'] == 0x15))

        if not oneTsv:
            logger.warn('procKLV() returned false, investigate?')
            return

        if (s['esType'] == 0x15):
            # Sync KLV? study relationship between PTS and KLV TS
            logger.debug('SYN PTS=%d, KLVTS=%d' %
                         (s['pts'], misbKLV.klvClock))
        else:
            # Async KLV? Maybe issue a warning
            if not asyWarned:
                logger.warn('WARNING: Async KLV mux method (less accurate)')
                asyWarned = True

        if (len(oneTsv)):
            tsvFile.write(oneTsv.encode('utf-8') + b'\n')


asyWarned = False

if (len(sys.argv) != 3):
    sys.exit('Syntax: ' + sys.argv[0] + ' infile.mpg outfile.tsv')

mpgFileName = sys.argv[1]
tsvFileName = sys.argv[2]

try:
    mpgFile = open(mpgFileName, mode='rb')
except:
    sys.exit('Unable to read from: ' + mpgFileName)

if os.path.isfile(tsvFileName):
    sys.exit('Output file exists: ' + tsvFileName)

try:
    tsvFile = open(tsvFileName, mode='wb')
except:
    sys.exit('Unable to write to: ' + tsvFileName)

logger = logging.getLogger()
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.INFO)

# Enable debug output
DEBUG_LEVEL = int(os.getenv('DEBUG', '0'))
if DEBUG_LEVEL:
    logger.setLevel(logging.DEBUG)

r = mpegTSlite.mpegTS()
r.onRxFullPES = myPesHandler


while True:
    tsPacket = mpgFile.read(mpegTSlite.PACKETSIZE)
    if len(tsPacket) > 0:
        r.ingest(tsPacket)
    else:
        break
