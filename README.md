# mpeg-ts-klv

Open source, pure Python modules and utilities for manipulating KLV formatted UAS metadata streams in MPEG-TS containers

## modules
- `mpegTSlite` - some MPEG-TS functions used by the KLV utilities; is NOT intended to be a complete MPEG-TS library (hence the suffix "lite")
- `mpegKLV` - functions for parsing and building MISB ST0601 KLV data blocks

## utilities
- `mpg2tsv` - extract KLV metadata from an MPEG-TS file into TSV format
- `tsv2klvi` - insert metadata (supplied in TSV format) into an MPEG-TS file as a KLV stream

## formats

### MPEG format
Standard MPEG-TS packetized at 188 bytes

### TSV format
A delimited text format, where each row represents one data packet, formatted as:
- TAB-separated columns
- first column contains the ordered list of the keys in the packet, comma-separated
- the rest of the columns contain the respective values corresponding to the keys

Example:

|Key|Value|
|---|---|
|2|1348086751347522|
|4|ANGRYBIRD|
|13|41.09982879|
|14|-104.79172264|
|15|2934.18731689|
|65|1|
|1|0x4321|

TSV representation:
`2,4,13,14,15,65,1<tab>1348086751347522<tab>ANGRYBIRD<tab>41.09982879<tab>-104.79172264<tab>2934.18731689<tab>1<tab>0x4321`

## using the utilities

### extract KLV metadata stream from an MPEG-TS file into TSV format
`mpg2tsv source.mpg output.tsv`

Open the resulting TSV file in Excel or OOCalc and convert or modify as needed

### add a KLV metadata stream to an MPEG-TS file
`tsv2klvi source.mpg klvdata.tsv output.mpg`

NOTE: the conversion ends when the shortest input file ends.

## using the modules
This section is work in progress; for now, please refer to the code comments, tests and examples at the bottom of each module file.

## status
*itWorksForMe*

## known issues
- overall code quality is ghetto
- insufficient error handling
- insufficient input sanitization
- documentation is incomplete

---
Copyright 2020 by Alec Murphy, MIT licensed | https://gitlab.com/almurphy
